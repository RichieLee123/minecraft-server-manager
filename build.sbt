name := "Minecraft-Server-Manager"

version := "0.1"

scalaVersion := "2.13.6"

val betterFilesVersion = "3.9.1"
val catsVersion = "2.3.0"
val catsEffectVersion = "3.1.1"
val sprayVersion = "1.3.6"
val sttpVersion = "3.3.18"
val circeVersion = "0.14.1"
val http4sVersion = "0.23.0-RC1"

val mainLibraries = Seq (
  "org.typelevel" %% "cats-core" % catsVersion,
  "org.typelevel" %% "cats-effect" % catsEffectVersion,

  "com.softwaremill.sttp.client3" %% "core" % sttpVersion,
  "com.softwaremill.sttp.client3" %% "async-http-client-backend-cats" % sttpVersion,
  "com.softwaremill.sttp.client3" %% "circe" % sttpVersion,

  "com.github.pathikrit" %% "better-files" % betterFilesVersion,

  "io.circe" %% "circe-core" % circeVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-parser" % circeVersion,
  "io.circe" %% "circe-optics" % circeVersion,

  "org.http4s" %% "http4s-dsl" % http4sVersion,
  "org.http4s" %% "http4s-blaze-server" % http4sVersion,
  "org.http4s" %% "http4s-blaze-client" % http4sVersion,

  "com.github.pureconfig" %% "pureconfig" % "0.17.1",
)

val catsEffectTestVersion = "1.1.1"
val scalaTestVersion = "3.2.9"
val mockitoVersion = "3.2.9.0"

val testLibraries = Seq(
  "org.scalatest" %% "scalatest" % scalaTestVersion % "test",
  "org.typelevel" %% "cats-effect-testing-scalatest" % catsEffectTestVersion % "test",
  "org.scalatestplus" %% "mockito-3-4" % mockitoVersion % "test"
)

libraryDependencies ++= mainLibraries ++ testLibraries