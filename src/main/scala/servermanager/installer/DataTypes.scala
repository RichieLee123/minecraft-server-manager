package servermanager.installer

case class VersionManifest(latest: LatestReleases, versions: List[MinecraftVersion])
case class LatestReleases(release: String, snapshot: String)
case class MinecraftVersion(id: String, url: String)
