package servermanager.installer

import servermanager.http.client.RequestManager
import cats.effect.IO
import sttp.client3.asynchttpclient.cats.AsyncHttpClientCatsBackend
import sttp.client3.{SttpBackend, _}
import io.circe._
import io.circe.parser._
import io.circe.optics.JsonPath._
import io.circe.generic.semiauto._

case class RequestError(error: String) extends Throwable(error)
case class PackageParsingError(uri: String) extends Throwable{
  override def getMessage: String = s"Could not find URI in Version Package: $uri"
}

trait VersionRetriever {
  def getLatestVersion: IO[MinecraftVersion]
}

trait decoders {
  implicit val versionManifestDecoder: Decoder[VersionManifest] = deriveDecoder[VersionManifest]
  implicit val latestReleasesDecoder: Decoder[LatestReleases] = deriveDecoder[LatestReleases]
  implicit val minecraftVersionDecoder: Decoder[MinecraftVersion] = deriveDecoder[MinecraftVersion]
}

object MojangVersionRetriever extends VersionRetriever with decoders {
  private[installer] def getLatestVersion(be: SttpBackend[IO, Any]): IO[MinecraftVersion] = {
    getManifest(be)
      .map { manifest => {
        val currentVersion = manifest.latest.release
        val versionUrl = manifest.versions.find(v => v.id == manifest.latest.release).map(_.url)
        IO.fromOption(versionUrl)(RequestError(s"unable to retrieve url for version $currentVersion"))
          .map(url => getVersionDownload(url)(be)).flatten
          .map(e => MinecraftVersion(currentVersion, e))
        }
      }.flatten
  }

  private[installer] val getManifest: SttpBackend[IO,Any] => IO[VersionManifest] = implicit backend =>
    RequestManager.makeRequest("https://launchermeta.mojang.com/mc/game/version_manifest.json")
      .map(s => decode(s)(versionManifestDecoder))
      .map(IO.fromEither(_)).flatten

  def getVersionDownload(versionUrl: String) (implicit backend: SttpBackend[IO, Any]): IO[String] =
    RequestManager.makeRequest(versionUrl)
      .map(parse(_).getOrElse(Json.Null))
      .map(root.downloads.server.url.string.getOption(_).getOrElse(throw PackageParsingError(versionUrl)))

  def getLatestVersion: IO[MinecraftVersion] =
    AsyncHttpClientCatsBackend[IO]().map(getLatestVersion).flatten
}
