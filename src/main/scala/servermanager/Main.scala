package servermanager

import cats.effect.{ExitCode, IO, IOApp}
import org.http4s.blaze.server.BlazeServerBuilder
import servermanager.http.server.Routes
import servermanager.ApplicationConfig.Config

import scala.concurrent.ExecutionContext.Implicits.global

object Main extends IOApp with Config {
  override def run(args: List[String]): IO[ExitCode] = {
    BlazeServerBuilder[IO](global)
      .bindHttp(8080, "localhost")
      .withHttpApp(Routes.apply)
      .resource
      .use(_ => IO.never) //generates the server as a resource and then hands it off the the function in use
      // the function in use calls io.never which is a non-terminating thread meaning that the server will never close.
      .as(ExitCode.Success)
  }
}
