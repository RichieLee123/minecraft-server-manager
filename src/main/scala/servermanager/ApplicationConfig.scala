package servermanager

import pureconfig.generic.ProductHint
import pureconfig.generic.auto._
import pureconfig.{ConfigFieldMapping, ConfigSource, SnakeCase}

object ApplicationConfig {
  implicit def SnakeCasedConfigHint[T <: SnakeCasedConfig]: ProductHint[T] = ProductHint(ConfigFieldMapping(SnakeCase, SnakeCase))

  trait SnakeCasedConfig

  case class RootConfig(
    application: Application
  ) extends SnakeCasedConfig

  case class Application(
    http_port: Int,
    minecraft_version_manifest_url: String
  ) extends SnakeCasedConfig

  trait Config {
    val config = ConfigSource.default.loadOrThrow[RootConfig]
  }
}