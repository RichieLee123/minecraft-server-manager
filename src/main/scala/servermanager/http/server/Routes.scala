package servermanager.http.server

import cats.effect.IO
import org.http4s.HttpRoutes
import org.http4s.dsl.io._
import org.http4s.implicits.http4sKleisliResponseSyntaxOptionT

object Routes {
  def apply = HttpRoutes.of[IO] {
    case GET -> Root / "install" / version =>
      Ok(s"installed version.$version..")
  }.orNotFound
}
