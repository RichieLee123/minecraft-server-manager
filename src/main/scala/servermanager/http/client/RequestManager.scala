package servermanager.http.client

import servermanager.installer.RequestError
import better.files.File
import cats.effect.IO
import sttp.client3.{SttpBackend, UriContext, asFile, basicRequest}

object RequestManager {
  def makeRequest(url: String)(implicit backend: SttpBackend[IO, Any]): IO[String] =
    basicRequest.get(uri"$url")
      .send(backend)
      .flatMap(r => IO.fromEither(r.body.left.map(RequestError)))

  def fileRequest(url: String, targetDirectory: String)(implicit backend: SttpBackend[IO, Any]): IO[File] = {
    val file = File(targetDirectory)
    basicRequest.get(uri"$url")
      .response(asFile(file.toJava))
      .send(backend)
      .flatMap(r => IO.fromEither(r.body.map(_ => file).left.map(RequestError)))
  }
}
