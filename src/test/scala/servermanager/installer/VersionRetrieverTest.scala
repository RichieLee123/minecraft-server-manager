package servermanager.installer

import cats.effect.IO
import cats.effect.testing.scalatest.AsyncIOSpec
import org.scalatest.flatspec.AsyncFlatSpec
import org.scalatest.matchers.should._
import sttp.client3.UriContext
import sttp.client3.asynchttpclient.cats.AsyncHttpClientCatsBackend

import scala.io.{BufferedSource, Source}

trait TestFiles {
  def testFile(name: String): String = {
    val file: BufferedSource = Source.fromURL(getClass.getResource(s"/$name"))
    val fileString = file.getLines().flatten.mkString("")
    file.close()
    fileString
  }
}

class VersionRetrieverTest extends AsyncFlatSpec with AsyncIOSpec with Matchers with TestFiles {
  behavior of "MojangVersionRetriever"
  val validManifest: String = testFile("validManifest.json")
  val validVersionPackage: String = testFile("validVersionPackage.json")
  val testManifestUri = uri"https://launchermeta.mojang.com/mc/game/version_manifest.json"

  it should "Return the latest version if it can retrieve a valid Manifest" in {
    val backendStub = AsyncHttpClientCatsBackend.stub[IO]
      .whenRequestMatches(_.uri.equals(testManifestUri))
      .thenRespond(validManifest)
      .whenRequestMatches(_.uri.equals(uri"https://launchermeta.mojang.com/v1/packages/69051df81e8cd61a292291b5953d783cd38203e8/1.17.json"))
      .thenRespond(validVersionPackage)

    val res = MojangVersionRetriever.getLatestVersion(backendStub)
    res.asserting(_ shouldBe MinecraftVersion("1.17", "https://launcher.mojang.com/v1/objects/0a269b5f2c5b93b1712d0f5dc43b6182b9ab254e/server.jar"))
  }

  it should "Return a failure if it cannot connect" in {
    val backendStub = AsyncHttpClientCatsBackend.stub[IO]
      .whenRequestMatches(_.uri.equals(testManifestUri))
      .thenRespondServerError()

    val res = MojangVersionRetriever.getLatestVersion(backendStub)
    res.attempt.asserting(_ shouldBe Left(RequestError("Internal server error")))
  }

  it should "Return a failure if it cannot parse the manifest" in {
    val badResponse = "Oops!... i did it again to your heart"
    val backendStub = AsyncHttpClientCatsBackend.stub[IO]
      .whenRequestMatches(_.uri.equals(testManifestUri))
      .thenRespond(badResponse)

    val res = MojangVersionRetriever.getLatestVersion(backendStub)
    res.attempt.asserting(e => {
      val result = e.swap.getOrElse(throw new NoSuchElementException("result did not throw"))
      result.getMessage shouldBe "expected json value got 'Oops!....' (line 1, column 1)"
    })
  }

  it should "return a failure if it cannot parse the versionPackage" in {
    val packageUri = "https://launchermeta.mojang.com/v1/packages/69051df81e8cd61a292291b5953d783cd38203e8/1.17.json"
    val badResponse = "got lost, in this game oh baby"
    val backendStub = AsyncHttpClientCatsBackend.stub[IO]
      .whenRequestMatches(_.uri.equals(testManifestUri))
      .thenRespond(validManifest)
      .whenRequestMatches(_.uri.equals(uri"$packageUri"))
      .thenRespond(badResponse)

    val res = MojangVersionRetriever.getLatestVersion(backendStub)
    res.attempt.asserting(e => {
      val result = e.swap.getOrElse(throw new NoSuchElementException("result did not throw"))
      result shouldBe PackageParsingError(packageUri)
    })
  }
}
